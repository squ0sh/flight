# Project_2

#### Air Traffic Data sources:

* https://openflights.org/data.html#airport

* https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat

* https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json

* https://www.adsbexchange.com/

* https://www.adsbexchange.com/data/#

* https://www.airport-data.com/airport/36TS/

* http://www.virtualradarserver.co.uk/Documentation/Formats/AircraftList.aspx

* http://www.flyingnut.com/adsbmap/adsbObs.php

* http://www.flyingnut.com/adsbmap/

#### Weather API:




---

#### Next Steps:

* Connect a database and query the main API every x seconds to keep track of the aircrafts movement.

* Associate the aircrafts with the airports (to/from).

* Track the airports traffic.

* Insert dropdown menus for user interaction.

* Add layers for aircrafts, airports, weather, etc.

* Add a search field to the user search by aircraft or airport

* Add stats for individual flights and airports



